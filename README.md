Into:
---
This is a guide for transitioning a LEMP stack that is designed for development from Docker to Rkt (Rocket, from CoreOS). It is a work in process. 
At the end of this we will have something resembling the convenient "docker-compose up" command but instead be using rkt. "Why?" You may ask. Well of course, if we don't break things we will never learn.

Commands to get started:
---
rkt

rkt fetch --insecure-options=image docker://nginx

rkt run --net=host docker://nginx

If you navigate to localhost in your web browser you will see the default Nginx page. 

rkt run --net=host --volume data,kind=host,source=$HOME/www docker://nginx --mount volume=data,target=/usr/share/nginx/html

Assuming you are Linux and you have a directory called www in your home directory, make an index.html page in wwww. Navigate to localhost again to see this page served by nginx.



Rkt is:
---
more flexible, more open, and more secure than Docker while maintaining the efficiency that Docker has over other solutions such as Vagrant. Rather than start an entire system up for each container, only what is required is loaded.

From the official rkt:
https://coreos.com/rkt/docs/latest/
rkt is an implementation of the App Container spec. rkt's native image format (ACI) and runtime/execution environment (pods) are defined in the specification.

The app container spec is one approach to Linux containers, allowing for streamlined development between different developers and different machines.

Pulled directly from the App Container Github page:
https://github.com/appc/spec

What is the App Container spec?
App Container (appc) is a well-specified and community developed specification for application containers. appc defines several independent but composable aspects involved in running application containers, including an image format, runtime environment, and discovery mechanism for application containers.
What is an application container?
An application container is a way of packaging and executing processes on a computer system that isolates the application from the underlying host operating system. For example, a Python web app packaged as a container would bring its own copy of a Python runtime, shared libraries, and application code, and would not share those packages with the host.
Application containers are useful because they put developers in full control of the exact versions of software dependencies for their applications. This reduces surprises that can arise because of discrepancies between different environments (like development, test, and production), while freeing the underlying OS from worrying about shipping software specific to the applications it will run. This decoupling of concerns increases the ability for the OS and application to be serviced for updates and security patches.
For these reasons we want the world to run containers, a world where your application can be packaged once, and run in the environment you choose.
The App Container (appc) spec aims to have the following properties:
Composable. All tools for downloading, installing, and running containers should be well integrated, but independent and composable. 
Secure. Isolation should be pluggable, and the cryptographic primitives for strong trust, image auditing and application identity should exist from day one. 
Decentralized. Discovery of container images should be simple and facilitate a federated namespace and distributed retrieval. This opens the possibility of alternative protocols, such as BitTorrent, and deployments to private environments without the requirement of a registry. 
Open. The format and runtime should be well-specified and developed by a community. We want independent implementations of tools to be able to run the same container consistently. 

Rkt is designed to work with init systems like systemd. 



According to Fedora Magazine:
Your computer’s startup or boot process begins with the BIOS (Basic Input / Output System) software on the motherboard. After completing hardware initialization and checks, the BIOS starts up the bootloader (GRUB in Fedora’s case). The bootloader accesses the Master Boot Record (MBR) on your storage device, usually a hard drive. It uses the data there to locate and start the Linux kernel.
The init system is the first process started on Fedora after the kernel starts. In fact, the init system always gets the Process ID (PID) of 1 on a system. This process is always executed by the Linux kernel after the early stages of bootup are completed by the BIOS and bootloader (GRUB).
----
Long start short it handles the initialization of various operating system tasks. Systemd is a broad topic. We will keep to the specifics of managing rkt through only the necessary commands.